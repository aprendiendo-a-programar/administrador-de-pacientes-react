import { useState, useEffect } from "react";
import Error from "./Error";
import Patien from "./Patien";

const Form = ({ patients, setPatiens, patient, setPatient }) => {
    const [name, setName] = useState('');
    const [owner, setOwner] = useState('');
    const [email, setEmail] = useState('');
    const [date, setDate] = useState('');
    const [symptom, setSymptom] = useState('');
    const [error, setError] = useState(false);

    useEffect(() => {
        // Comprobar si el array está vacío
        if(Object.keys(patient).length > 0) {
            setName(patient.name);
            setOwner(patient.owner);
            setEmail(patient.email);
            setDate(patient.date);
            setSymptom(patient.symptom);
        } 
    }, [patient])

    const genId = () => {
        const random = Math.random().toString(36).substr(2);
        const date = Date.now().toString(36);

        return (random + date);
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        if ([name, owner, email, date, symptom].includes('')) {
            setError(true);

            setTimeout(() => {
                setError(false);
            }, 4000);

            return;
        }

        setError(false);

        /* Objeto de pacientes */
        const objetPatiens = {
            name,
            owner,
            email,
            date,
            symptom,
        }

        if(patient.id) {
            // Editando registro
            objetPatiens.id = patient.id;
            const patientsUpdate = patients.map( patientState => patientState.id === patient.id ? objetPatiens : patientState );

            setPatiens(patientsUpdate);
            setPatient({});
        } else {
            // Nuevo registro
            objetPatiens.id = genId();
            setPatiens([...patients, objetPatiens]);
        }


        /* Reiniciar form */
        setName("")
        setOwner("");
        setEmail("");
        setDate("");
        setSymptom("");

        /* Ir hacia abajo */

    }

    return (
        <div className="md:w-1/2 lg:w-2/5 px-3 relative">
            <h2 className="font-black text-3xl text-center">Seguimiento pacientes</h2>
            <p className="text-lg mt-5 text-center mb-10">Añade pacientes y <span className="text-indigo-600 font-bold"> Administralos</span></p>

            <form
                onSubmit={handleSubmit}
                className={`bg-white shadow-md rounded-md py-10 px-5 sticky top-10 ${Object.keys(patient).length && "animate__animated animate__flipInY"}`}
            >
                <div className="mb-5">
                    <label htmlFor="mascota" className="block text-gray-700 uppercase font-bold">Nombre mascota</label>

                    <input
                        id="mascota"
                        type="text"
                        placeholder="Nombre de la mascota"
                        className="border-2 border-gray-200 w-full p-2 mt-2 placeholder-gray-400 rounded-md outline-indigo-600"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                </div>

                <div className="mb-5">
                    <label htmlFor="owner" className="block text-gray-700 uppercase font-bold">Nombre del dueño</label>

                    <input
                        id="owner"
                        type="text"
                        placeholder="Nombre del dueño"
                        className="border-2 border-gray-200 w-full p-2 mt-2 placeholder-gray-400 rounded-md outline-indigo-600"
                        value={owner}
                        onChange={(e) => setOwner(e.target.value)}
                    />
                </div>

                <div className="mb-5">
                    <label htmlFor="email" className="block text-gray-700 uppercase font-bold">Email</label>

                    <input
                        id="email"
                        type="email"
                        placeholder="Ej: dueño@gmail.com"
                        className="border-2 border-gray-200 w-full p-2 mt-2 placeholder-gray-400 rounded-md outline-indigo-600"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </div>

                <div className="mb-5">
                    <label htmlFor="date" className="block text-gray-700 uppercase font-bold">Alta</label>

                    <input
                        id="date"
                        type="date"
                        className="border-2 border-gray-200 w-full p-2 mt-2 placeholder-gray-400 rounded-md outline-indigo-600"
                        value={date}
                        onChange={(e) => setDate(e.target.value)}
                    />
                </div>

                <div className="mb-5">
                    <label htmlFor="symptom" className="block text-gray-700 uppercase font-bold">Síntomas</label>

                    <textarea
                        id="symptom"
                        placeholder="Síntomas de la mascota"
                        className="border-2 border-gray-200 w-full p-2 mt-2 placeholder-gray-400 rounded-md outline-indigo-600"
                        value={symptom}
                        onChange={(e) => setSymptom(e.target.value)}
                    />
                </div>

                {error &&
                    <Error message="Todos los campos son obligatorios" />
                }

                <input
                    type="submit"
                    className="bg-indigo-600 w-full p-3 text-white uppercase font-bold cursor-pointer rounded-md hover:bg-indigo-800 hover:shadow-xl transition-all"
                    value={ patient.id ? "Guardar cambios" : "Agregar paciente"}
                />
            </form>
        </div>
    );
}

export default Form;