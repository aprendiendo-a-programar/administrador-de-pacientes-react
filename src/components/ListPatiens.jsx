import Patien from "./Patien";

const ListPatiens = ({ patients, setPatient, deletePatient }) => {

    return (
        <div className="md:w-1/2 lg:w-3/5 mt-5 md:mt-0">

            {
                patients.length ?
                    <>
                        <h2 className="font-black text-3xl text-center">Lista de pacientes</h2>
                        <p className="text-lg mt-5 text-center mb-10">
                            Administra tus <span className="text-indigo-600 font-bold">Pacientes y Citas</span>
                        </p>
                    </>
                    :
                    <>
                        <h2 className="font-black text-3xl text-center">No hay pacientes</h2>
                        <p className="text-lg mt-5 text-center mb-10">
                            <span className="text-indigo-600 font-bold">Rellena el formulario para agregar pacientes</span>
                        </p>
                    </>
            }
            {
                patients.map((patient) => {
                    return (
                        <Patien
                            key={patient.id}
                            patient={patient}
                            setPatient={setPatient}
                            deletePatient={deletePatient}
                        />
                    )
                })
            }

        </div>
    );
}

export default ListPatiens;