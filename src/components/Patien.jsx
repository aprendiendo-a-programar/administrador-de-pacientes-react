import 'animate.css';

const Patien = ({ patient, setPatient, deletePatient }) => {

    const handleDelete = () => {
        const response = confirm("¿Estás seguro/a de que quieres eliminar este paciente?");

        if(response) {
            deletePatient(patient.id)
        }
    }

    return (
        <div className="m-3 mb-5 bg-white shadow-md px-5 py-5 rounded-md animate__animated animate__bounceInUp">
            <p className="font-bold mb-3 text-gray-700 uppercase">
                Nombre: <span className="font-normal normal-case"> {patient.name}</span>
            </p>

            <p className="font-bold mb-3 text-gray-700 uppercase">Propietario:
                <span className="font-normal normal-case"> {patient.owner}</span>
            </p>

            <p className="font-bold mb-3 text-gray-700 uppercase">Email:
                <span className="font-normal normal-case"> {patient.email}</span>
            </p>

            <p className="font-bold mb-3 text-gray-700 uppercase">Fecha de alta:
                <span className="font-normal normal-case"> {patient.date}</span>
            </p>

            <p className="font-bold mb-3 text-gray-700 uppercase">Síntomas:
                <span className="font-normal normal-case"> {patient.symptom}</span>
            </p>

            <div className="flex justify-between mt-10">
                <button
                    type="button"
                    className="py-2 px-10 bg-indigo-600 hover:bg-indigo-700 text-white uppercase font-bold rounded-md cursor-pointer"
                    onClick={() => setPatient(patient)}
                >Editar</button>

                <button
                    type="button"
                    className="py-2 px-10 bg-red-600 hover:bg-red-700 text-white uppercase font-bold rounded-md cursor-pointer"
                    onClick={handleDelete}
                >Eliminar</button>
            </div>
        </div>
    );
}

export default Patien;