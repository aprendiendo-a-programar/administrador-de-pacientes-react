import { useState, useEffect } from 'react';
import Form from './components/Form';
import Header from './components/Header';
import ListPatiens from './components/ListPatiens';

const App = () => {
  const [patients, setPatiens] = useState([]);
  const [patient, setPatient] = useState({});

  useEffect(() => {
    const getLocalStorage = () => {
      const patientsLS = JSON.parse(localStorage.getItem('patients')) ?? [];

      setPatiens(patientsLS);
    }

    getLocalStorage();
  }, [])

  useEffect(() => {
    localStorage.setItem("patients", JSON.stringify(patients));
  }, [patients])

  const deletePatient = (id) => {
    const patientsUpdated = patients.filter( patient => patient.id !== id);

    setPatiens(patientsUpdated);
  }

  return (
    <div className="container mx-auto mt-20">
      <Header />

      <div className="mt-12 md:flex">
        <Form 
          patients={patients}
          setPatiens={setPatiens}
          patient={patient}
          setPatient={setPatient}
        />
        <ListPatiens 
          patients={patients}
          setPatient={setPatient}
          deletePatient={deletePatient}
        />
      </div>
    </div>
  )
}

export default App
